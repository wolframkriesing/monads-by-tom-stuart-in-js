const expect = require('unexpected');
const { describe, it } = require('mocha');


const syntacticSugar = (value, thing) => new Proxy(thing, {
  has: (target, property) => property in target || property in value,
  get: (target, property) => property in target ? target[property] : target.and_then(x => x[property]),
});


const NullObject = syntacticSugar(null, {
  and_then: () => NullObject
});

const Optional = value => {
  if (value === NullObject || value == null) {
    return NullObject;
  }
  return syntacticSugar(value, {
    and_then: (fn) => {
      let result = fn(value);
      return result ? Optional(result) : NullObject
    }
  });
};



describe('Optional', () => {

  it('when given empty, returns NullObject', () => {
    let optional = Optional({}).and_then(x => x.whatever);
    expect(optional, 'to be', NullObject)
  });

  it('when given an object, can access the property', () => {
    const optional = Optional({ property: 'value' });
    expect(optional.and_then(x => x.property), 'to be Some', 'value')
  });

  it('when given the NullObject, returns the NullObject', () => {
    let optional = Optional(NullObject);
    expect(optional, 'to be', NullObject);
  });

  it('when given undefined, returns the NullObject', () => {
    expect(Optional(undefined), 'to be', NullObject);
  });

  it('when given null, returns the NullObject', () => {
    expect(Optional(null), 'to be', NullObject);
  });

});

describe('works?', () => {
  let weather_for = function(project) {
    return Optional(project).creator.address.country.capital.weather;

  };

  it('weather for a project, where all deps are existing', () => {
    const project = {
      creator: {
        address: {
          country: {
            capital: {
              weather: 'sunny'
            }
          }
        }
      }
    };

    expect(weather_for(project), 'to be Some', 'sunny')
  });

  // >> bad_project = Project.new(Person.new(Address.new(nil)))
  it('given a person without address, returns null', () => {
    const project = {
      creator: {
        address: null
      }
    };
    let weather = weather_for(project);
    expect(weather, 'to be', NullObject);
  });

  it('given a null project, returns null', () => {
    expect(weather_for(NullObject), 'to be', NullObject);
  });
});

expect.addType({
  name: 'Optional',
  identify: object => object.hasOwnProperty('and_then')
});

expect.addAssertion('<Optional> to be Some <any>', function(pen, actual, expected) {
  let actualValue = null;
  actual.and_then(x => {actualValue = x});
  expect(actualValue, 'to be', expected);
});