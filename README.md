# Monads by Tom Stuart in JavaScript

This repo got triggered by [Tom Stuart's talk "Refactoring Ruby with Monads"][talk], to implement
the use cases he talks about in his talk just in JavaScript (not in Ruby).
This started at [#busconf17].

## Setup

### Using nix

If you don't have [nix as package manager][nix] yet, [install it now][install-nix]! Imho it's the light weight
version of docker :).

- `cd monads-by-tom-stuart-in-js` change into this project's directory
- `nix-shell` start the nodejs8 env
- `npm i` install all the needed packages
- `npm test` runs all type checks, builds and runs the tests

### Without using nix

Make sure to have a later nodejs (works with v8) version installed.

- `cd monads-by-tom-stuart-in-js` change into this project's directory
- `npm i` install all the needed packages
- `npm test` runs all type checks, builds and runs the tests

[talk]: http://codon.com/refactoring-ruby-with-monads
[#busconf17]: https://twitter.com/search?q=%23busconf17
[install-nix]: https://nixos.org/nix/manual/#chap-quick-start
[nix]: https://nixos.org/nix/